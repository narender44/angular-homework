import { Component, OnInit } from '@angular/core';
import {DataService} from "../data.service";
import {Position} from "../entities/position";
import {generate} from "rxjs";
import {Department} from "../entities/department";

@Component({
  selector: 'app-positions',
  templateUrl: './positions.component.html',
  styleUrls: ['./positions.component.css']
})
export class PositionsComponent implements OnInit {
  private position: Position;



  constructor(public dataService: DataService) { }

  ngOnInit(): void {
    this.getPositions();
  }

  getPositions(): void {
    this.dataService.getPositions().subscribe(positions => this.dataService.positions = positions);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    // @ts-ignore
    this.dataService.addPosition({title: name})
      .subscribe(position => {
        this.dataService.positions.push(position);
      });
  }


  delete(position: Position) {
    this.dataService.deletePosition(position.id).subscribe(result => {
      this.dataService.positions = this.dataService.positions.filter(item => item.id !== position.id);
    });
  }

  edit(position: Position) {
    this.dataService.positionId = position.id;
    document.getElementById("position").style.display = "block";
  }

  save(value: string) {
    this.dataService.getPosition(this.dataService.positionId).subscribe(data => {
      this.position = data;
      this.position.title = value;
      this.dataService.savePosition(this.position);
      this.dataService.getPositions().subscribe(data1 => this.dataService.positions = data1);
    });
    this.dataService.positionId = null;
    this.position = null;
  }
  //
  cancel() {
    this.dataService.positionId = null;
    this.dataService.positionId = null;
    this.position = null;
    document.getElementById("position").style.display = "none";
  }

  hide(){
    document.getElementById("position").style.display = "none";
  }

}



